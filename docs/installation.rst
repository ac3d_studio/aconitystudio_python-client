Installation
============

There are two ways to install AconitySTUDIO Python Client, directly from a python wheel or by cloning the repository.

From sources
--------------

First, download the wheel from the `Bitbucket repo download tab`_. Next, execute

    $ pip install AconitySTUDIO_client_wheelname.whl

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/
.. _Bitbucket repo download tab: https://bitbucket.org/ac3d_studio/aconitystudio_python-client/downloads/  

From Bitbucket
---------------

The sources for AconitySTUDIO Python Client can be downloaded from the `Bitbucket repo`_.

Clone the public repository:

.. code-block:: console

    $ git clone git@bitbucket.org:ac3d_studio/AconitySTUDIO_client.git

Then, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.com/ac3d_blom/AconitySTUDIO_client
.. _tarball: https://bitbucket.com/ac3d_blom/AconitySTUDIO_client/tarball/master
