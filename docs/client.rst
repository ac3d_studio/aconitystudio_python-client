Guide
============================

In depth documentation of the functionality of the AconitySTUDIO Python Client.

REST API
-----------------------------

Controlling the machine is done via a REST API.

A REST API uses HTTP protocol, where a client can send requests and get responses
from the server. Here, the server is the AconitySTUDIO, whereas this program is the client.
The basic requests from HTML which are already supported with the Python Client are

 * get
 * post
 * put

The REST API is documented is detail in the `Custumer Support of Aconity3D`_ 
(5.0 How Tos > AconitySTUDIO REST-API 1.0 RC)


.. _Custumer Support of Aconity3D: https://aconity3d.atlassian.net/wiki/spaces/CUS/pages/566231073/AconitySTUDIO+REST-API+Version+1.0+RC

Example usage of the API::

    functions = await client.get('machines/functions')

Note that no IP adress and port are given above.

They get specified once during the setup of the client and are automatically appended in the request.
If the `rest_url` parameter (see section on client creating below) was specified to be 'http://192.168.1.1:9000',
the actual html request would be sent to the URL http://192.168.1.1:9000/machines/functions.

Creating the client
----------------------------

To create the client, use::

    from AconitySTUDIO_client import AconitySTUDIOPythonClient

    login_data = {
        'rest_url' : 'http://192.168.1.1:9000',
        'ws_url' : 'ws://192.168.1.1:9000',
        'email' : 'admin@yourcompany.com',
        'password' : '<password>'
    }

    client = await AconitySTUDIOPythonClient.create(login_data)

This will do a few things automatically
 * get a login cookie from the server (necessary for using the REST API).
 * create a websocket connection (useful for getting machine data through topics and reports).
 * set up a perioding ping, keeping the connection alive.

 
.. note::
    The Python client makes heavy use of concurrent programming using `asyncio`_ and other libraries. Understanding concurrency is important for understanding the the Python Client and especially so for adding new functionality which you might need.

.. _asyncio: https://docs.python.org/3/library/asyncio.html

Configuring the client
----------------------------

Imagine you want to manipulate one of your jobs with the Python Client.
Each of these jobs has a unique indentifier (which has nothing to do with the job's name).
To interact with the correct job we must know this job identifier.

It is visible in the browser based client in the url field::

    http://192.168.1.1:9000/#/jobs/5c4bg4h21a00005a00581012/editor

In this case, the job id is ``5c4bg4h21a00005a00581012``. If the job name is unique, a convenience
function exists to automatically gather and set the correct job id for the Python Client.

For example, if your job is named 'TestJob' use

::

    await client.get_job_id('TestJob')

This will automatically create an attribute ``job_id``, setting it to ``5c4bg4h21a00005a00581012``. 
From now on, if any method of the Python Client would require a job id, you can omit this argument in the function call.
If you chose to explicitly fill in this parameter in a function call,
the clients own attribute (if it exists at all) will be ignored.

For normal operation of the Python Client, the id's of the configuration 
and the machine itself must be known aswell.

If the names are unique, they can be easily retrieved in the same way, using::

    await client.get_machine_id('my_unique_machine_name')
    await client.get_config_id('my_unique_config_name')


If multiple machines, configurations or jobs exist with the same name,
they need to be looked up in the browser url field and given to the Python Client manually:

::

    client.job_id = '5c4bg4h21a00005a00581012'
    client.machine_id = 'your_machine_id_gathered_from_browser_url_bar'
    client.config_id = 'your_config_id_gathered_from_browser_url_bar'


Script execution
------------------------------

Simple things like turning on the process chamber illumination or manually moving
the slider can be done with the ``execute`` coroutine. For example, to turn on the light use

::

    light_on = '$m.on($c[light])'
    await client.execute(channel='manual', script=light_on)

Moving the slider can be accomplished via

::

    movement = '$m.move_rel($c[slider], -180)'
    await client.execute(channel='manual_move', script=movement)

Note that these commands get executed on different channels.
If a channel is occupied, any command sent to that channel will be ignored.

The ``execute`` coroutine takes care of this insofar that if you `await` it, it will only yield control to its caller once the channel is free again.
(This is accomplished by continually listening to the `run report` via a websocket.) Of course, this functionality can be easily commented out in
the source code of the `execute` coroutine or it can be bypassed by scripting

::

    movement = '$m.move_rel($c[slider], -180)'
    mover = asyncio.create_task(client.execute(channel='manual_move', script=movement))
    # we do something else now right after sending the request, not waiting for the function to yield back control.


To learn more about script execution, please try the example file `AconitySTUDIO_turn_light_on_and_off.py`.

Job Management
------------------------------

There are a number of functions to conveniently manage jobs.
This includes starting, pausing, resuming and stopping.

For starting a job, we need to specify the job id, an execution script and give information
about which layers shall be built with which parts. If we have set the attribute ``job_id``
and all parts should be built, a job can be started like this::

    start_layer = 1
    end_layer = 3
    layers = [start_layer, end_layer] #build layer 1,2,3
    build_parts = 'all'

    execution_script = \
    '''layer = function(){
    for(p:$p){
        $m.expose(p[next;$h],$c[scanner_1])
    }
    $m.add_layer($g)
    }
    repeat(layer)'''

    await start_job(layers, execution_script, build_parts)


.. note::
    The ``start_job`` coroutine does not take care of starting a config or importing parameters from the config into a job.
    This needs to be done in the GUI beforehand. Of course, it is always possible to do the basic job configuration via the REST API in the Python Client, but no convenience functions exist to simplify these tasks.
    At the moment, the Python Client is intended to be used alongside the GUI Version which can be used to configure a job in an easy way.
    
To change the parameters of a running job, all executions first have to be paused. This can be accomplished with ``await client.pause_job()``.

Example: The subpart *001_s1_vs* shall be exposed with a different Laserpower.

Changing this parameters involes::

    part_id = 1 #part_id of 001_s1_vs.
    param = 'laser_power'
    new_laser_power = 123
    await client.change_part_parameter(part_id, param, new_laser_power)

The ``part_id`` of a subpart can be gathered with the help of the GUI. In the job viewer, expand the parts and use the number inside the brackets.

Changing a global parameter can be done via::

    param = 'supply_factor'
    new_value = 2.2
    await client.change_global_parameter(param, new_value)

When we want to resume the job, we first need to know the new starting layer (current layer)::

    new_starting_layer = client.job_info['start_layer'] + client.job_info['AddLayerCommands']
    layers = [new_starting_layer, end_layer]
    await client.resume_job(layers, build_parts)

To know how many AddLayerCommands have been called a websocket connection is used in the background. Everytime another AddLayer Command is executed, the server sents a notification of that event to all subscribers.

You can test the job management with the help of `examples/AconitySTUDIO_AconitySCRIPT.py`.

Documentation of all functions
------------------------------------
.. autoclass:: AconitySTUDIO_client.AconitySTUDIO_client.AconitySTUDIO_client
    :members:


