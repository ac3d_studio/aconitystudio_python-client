import asyncio

import sys

import logging

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient

async def execute(login_data, info):
    '''
    1 Setup python client
    2 Get Batch data
        - every x seconds (sleep_time) we make a request to the server checking for new data
        - if new data arrived, we download it and save it locally

    Note: the folder structure from the server where the data is located is replicated for local use.
    '''
    print('\n\n ######## INIT CLIENT ########\n\n')

    #create client with factory method
    client = await AconitySTUDIOPythonClient.create(login_data, studio_version=info['studio_version'])

     # #### LOG SETUP #### #

    # * Create a logfile for this session in the example folder.
    # * The logfile contains the name of this script with a timestamp.
    # * Note: This is just one possible way to log.
    # * Logging configuration should be configured by the user, if any logging is to be used at all.

    client.log_setup(sys.argv[0], directory_path='')

    # #### SESSION STATE #### #

    print('\n\n ######## SESSION STATE ########\n\n')

    session_id = await client.gateway.get_session_id(n=-1) # this is the last session in time!

    job_id = await client.job.get_job_id(info['job_name'])
    machine_id = await client.gateway.get_machine_id(info['machine_name'])
    config_id = await client.gateway.get_config_id(info['config_name'])

    # #### Get Batch data #### #

    topics = info['topics']
    sleep_time = info['sleep_time']
    quality = info['quality']

    batch_infos = set([])

    while True:

        batch_infos_new = await client.data.get_download_urls(session_id, config_id, job_id, topics)

        #remove the old information from data already downloaded.
        batch_infos_changed = batch_infos_new - batch_infos

        logging.info(f'found {len(batch_infos_changed)} (possibly) new files to download')
        #print(f'NEW STUFF ({len(batch_infos_changed)}):', batch_infos_changed)

        url_appendix = f'/quality/{quality}'
        path_prefix = f'quality_{quality}%'

        await client.data.download_batch_data(batch_infos_changed, info['base_path_pyrometer_data'], url_appendix, path_prefix)

        await asyncio.sleep(sleep_time)

        batch_infos = batch_infos_new

#########################################################################################################################

if __name__ == '__main__':
    '''
    Example on how to use the python client to continually get pyrometer data data.
    Please change login_data and machine_info to your needs.
    '''

    #change login_data to your needs
    login_data = {
        'rest_url' : 'http://localhost:9000',
        'ws_url' : 'ws://localhost:9000',
        'email' : 'admin@aconity3d.com',
        'password' : 'passwd'
    }

    #change info to your needs

    info = {
        'machine_name' : 'SimMIDI',
        'config_name' : 'AconityMidi_Three_Scanner_Simulator',
        'job_name': 'python_script_job',
        'studio_version' : 2,
        'base_path_pyrometer_data' : 'D:/PyrometerData/', #where to save the data locally
        'topics': ['2Pyrometer'], #what type of data should be downloaded? (only 2Pyrometer and profile exist)
        'sleep_time': 10, #time between different checks of new data
        'quality' : 10, # the original data will be downsampled to {quality} percent.
    }

    result = asyncio.run(execute(login_data, info), debug=True)