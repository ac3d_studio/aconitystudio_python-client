import asyncio

import sys

import logging

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

logger = logging.getLogger(__package__)

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient

async def pauser(pause, message):

    '''helper function to put messages on the console'''

    # Pause the job.
    # (The pauser coroutine does a simple asyncio.sleep,
    # but with a countdown printed on the console.)

    print('')

    for i in range(pause):

        sys.stdout.write(f'\rWaiting for {pause-i} seconds {message}')

        sys.stdout.flush()

        #simulating waiting time
        await asyncio.sleep(1)

    print('')

async def predict():

    '''helper function to call the external program.'''

    print(f"PREDICT ACTIONS")

    await pauser(1, message='until the QS SYSTEM has done the analysis ...')

    # TODO
    # CALL QS SYSTEM with timeout (https://stackoverflow.com/questions/4238345/asynchronously-wait-for-taskt-to-complete-with-timeout)

    print(f"NEXT ACTIONS PREDICTED")

    # TODO DEFINE "ACTIONS" DEFINITION
    # (SUCCESS, ACTIONS)

    return (True, [])

async def prepare():

    '''helper function to start the external program.'''

    print(f"TRY TO PREPARE")

    await pauser(1, message='until prepared ...\n')

    print(f"PREPARED", flush=True)

    return True

async def unprepare():

    '''helper function to stop the external program.'''

    print(f"TRY TO UNPREPARE")

    await pauser(10, message='until unprepared ...')

    print(f"UNPREPARED")

    return True

def analyse_task_events(client, msg):

    if 'topic' in msg and (msg['topic'] == 'task' or msg['topic'] == 'cmds') :

        print(f"ANALYSE TASK EVENTS (msg={msg})\n", flush=True)

def analyse_machine_topic_events(client, msg):

    if 'topic' in msg and msg['topic'] != 'task' and msg['topic'] != 'run':

        print(f"ANALYSE DATA EVENTS (msg={msg})\n", flush=True)

def analyse_run_events(msg, channel):

    if 'topic' in msg and msg['topic'] == 'run':

        print(f"ANALYSE RUN EVENTS (channel={channel}, msg={msg})\n", flush=True)

async def execute_actions(job_id, client, layer_index):


    # ######## CHANGE GLOBAL PARAMETER ######## #


    # Next, we change one global parameter and two part specific parameters.
    # (To see the changed parameters in the GUI please reload the tab.)
    # await client.change_global_parameter('return_velocity', 113)


    # ######## CHANGE PART PARAMETER ######## #


    # Note: for the (first) argument of change_part_parameter, please read the documentation.

    # #### MARK SPEED #### #

    part_id = 1
    param = '0_master_panel_mark_speed'
    value = 4001
    await client.job.change_part_parameter(job_id = job_id, part_id = part_id, param=param, value=value)

    # # #### MARK SPEED #### #

    # part_id = 1
    # param = '0_master_panel_laser_power'
    # value = 100
    # await client.change_part_parameter(part_id = part_id, param=param, value=value)

    # #### SPOT DIAMETER #### #

    # part_id = 1
    # param = '0_master_panel_spot_diameter'
    # value = 0.08
    # await client.change_part_parameter(part_id = part_id, param=param, value=value)

    #await client.change_part_parameter(*change_2)
    #change_2 = (2, 'mark_speed', 456)
    print('\n\t***Parameters were changed. (To see the result in in the GUI, please reload the tab.)***\n\n')

    # ######## OTHER TASKS ######## #

    # taskHandler.on(client, "light")
    #### # taskHandler.on_value(client, "fume_extraction", 10)
    #### # taskHandler.off(client, "light")


    # taskHandler.move_rel(client, "slider", 10)
    # taskHandler.move_abs(client, "slider", 10)

    # client, target_temp, duration, p_gain, i_gain, d_gain
    # taskHandler.ramp_heating(client, 10, 10, 800, 0.01, 0.01)

    # taskHandler.add_layer(client)

    # #### EXPOSE #### #
    # layers = [layer_index, layer_index]
    # parts = [1, 2, 3]

    # taskHandler.expose(client, layers, parts)


    # await asyncio.sleep(2)

    # #### CHANGE USED PARTS #### #

    # client.job_info['used_parts'] = [1,2,3,4,5]
    parts = client.job.job_info['used_parts']

    await pauser(3, message='WAIT FOR ACTION')


    # ######## STOP JOB ######## #


    stop = False

    return (stop, parts)

async def execute(login_data, task):

    # '''
        # Example Code to illustrate on how an external program can be used to change parameter during a job.

    # * First: START JOB

    # The code does the following things:

    # * Gather necessary information about the machine and setup.
    # * Wait until state of machine is paused).
    # * Change one global and two part specific parameters.
    # * Resume the job.

    # new_layer = function(){
    #     ...
    # }

    # layer = function(){
    # for(p:$p){
    #     $m.expose(p[next;$h],$c[scanner_1])
    # }
    # pause(label)
    # $m.inc_h($g)
    # //$m.add_layer($g)
    # //new_add_layer()
    # }

    # repeat($n, layer)

    # '''

    # #### create client with factory method #### #

    client = await AconitySTUDIOPythonClient.create(login_data, studio_version=task['studio_version'])

    # #### LOG SETUP #### #

    # * Create a logfile for this session in the example folder.
    # * The logfile contains the name of this script with a timestamp.
    # * Note: This is just one possible way to log.
    # * Logging configuration should be configured by the user, if any logging is to be used at all.

    client.log_setup(sys.argv[0], directory_path='')


    # IMPORTANT:
    # the following convenience functions (get_job_id etc) only work if the job_name, machine_name or config_name are unique.
    # If this is not the case, set the attributes job_name, config_name, machine_name manually

    job_id = await client.job.get_job_id(task['job_name'])

    machine_id = await client.gateway.get_machine_id(task['machine_name'])
    config_id = await client.gateway.get_config_id(task['config_name'])
    channel = 'run0'

    # Information about the commands running on the Server.
    client.data.add_processor(['task', 'cmds'], analyse_task_events)

    await client.data.subscribe_event_topic('task')
    await client.data.subscribe_event_topic('cmds')

    # Machine Data topics

    # await client.data.subscribe_topic('State')
    # await client.data.subscribe_topic('Sensor')
    # await client.data.subscribe_topic('Positioning')

    # client.data.processors.append(analyse_machine_topic_events)


    # #### WAIT FOR QS SYSTEM START #### #

    await prepare()

    # #### EXECUTE FUNCTION UNTIL FINISHED #### #

    print('\n**************** WAIT UNTIL JOB STARTED ****************\n', flush=True)

    await client.execution.execute_until('started', analyse_run_events, channel=channel, number_of_checks = 1)

    await client.job.update_job_info()


    print('\n**************** START EXECUTION LOOP ****************\n', flush=True)


    wait = True

    layer_index = -1

    while wait is True:

        layer_index += 1

        print("\n**************** WAIT UNTIL JOB PAUSED ... ****************\n", flush=True)

        await client.execution.execute_until('paused', analyse_run_events, channel='run0', number_of_checks = 1)

        if client.execution.stopped[channel] == True or client.execution.finished[channel] == True:

            print("\n**************** JOB STOPPED OR FINISHED => END ANALYSIS ****************\n", flush=True)

            wait = False

        else:

            # #### QS SYSTEM PREDICT SOME ACTIONS #### #

            # prediction_result, prediction_actions = await predict()

            build_parts = 'all'

            # #### EXECUTE ACTIONS WITH CLIENT #### #

            # if prediction_result is True:

            (stop, build_parts) = await execute_actions(job_id, client, layer_index)

            # #### EITHER STOP JOB OR RESUME #### #

            if stop:

                print('\n**************** STOP JOB ****************\n')

                print(f"\n ******** JOB INFO {client.job.job_info} ********\n")

                await client.job.stop_job(job_id)

            else:

                # Resume the Job. We want to start at a different layer now, depending on how many layers we already built.

                print('\n**************** TRY RESUME ****************\n')

                print(f"\n ******** JOB INFO {client.job.job_info} ********\n")

                # new_starting_layer = (client.job_info['AddLayerCommands'] + 1)
                new_starting_layer = (layer_index) + client.job.job_info['original_start_layer']-1

                print(f"\n ******** TRY RESTART JOB (new_starting_layer={new_starting_layer}, end_layer={client.job.job_info['end_layer']}) ********\n")

                try:

                    await client.job.resume_job(job_id, layers=[new_starting_layer, client.job.job_info['end_layer']], parts=build_parts)

                except Exception as e:

                    if client.execution.stopped[channel] == True or client.execution.finished[channel] == True:

                        print("\n**************** JOB STOPPED OR FINISHED => END ANALYSIS ****************\n", flush=True)

                        wait = False

                    elif client.execution.paused[channel] == True:

                        print("\n**************** JOB still paused => END ANALYSIS ****************\n", flush=True)

                        print(f"\n ******** JOB INFO {client.job.job_info} ********\n")

                        await client.job.stop_job(job_id)

                        wait = False

                    else:

                        print("\n**************** JOB FAILED TO RESUME => END ANALYSIS ****************\n", flush=True)

                        print(f"\n ******** JOB INFO {client.job.job_info} ********\n")

                        await client.job.stop_job(job_id)

                        wait = False

    print('\n\t***we are done***\n\n', flush=True)

#########################################################################################################################

if __name__ == '__main__':

    '''

    Example script on how to

    * use the AconitySCRIPT example to control the flow of a job
    * and combine it with a second client to analyse and execute commands
    * start this example before the job is started

    While job is paused, we show how to change its parameters.

    '''

    # #### change login_data to your needs #### #

    login_data = {
        'rest_url' : f'http://localhost:9000',
        'ws_url' : f'ws://localhost:9000',
        'email' : 'admin@aconity3d.com',
        'password' : 'passwd'
    }

    # #### change info to your needs #### #

    # info = {
    #     'machine_name' : 'SIM',
    #     'config_name': 'ACONITYMIDIplus_Simulation_Multi',
    #     'job_name': 'python_test',

    #     # AconitySTUDIO version 1.x.x or 2.x.x (or greater)?
    #     'studio_version' : 2
    # }

    info = {
        'machine_name' : 'Sim',
        'config_name': 'Synced_Multiscanner_Simulator',
        'job_name': 'python_test',

        # AconitySTUDIO version 1.x.x or 2.x.x (or greater)?
        'studio_version' : 2
    }

    result = asyncio.run(execute(login_data, info), debug=True)
