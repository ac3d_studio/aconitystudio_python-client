import asyncio

import sys

import logging

import json

logger = logging.getLogger(__package__)

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient

machine_name_ = None

def handle_machine_state_event(topic, msg):

	state = msg['state']

	for event in msg['events']:

		cid = event['cid']

		if cid == machine_name_:

			logger.info(f"MMMM MACHINE STATE EVENT (machine_state={state}, event_cid={cid}, event_state={event['state']}, event_transition_state={event['t_state']})")

		else:

			logger.info(f"MMMM BASE COMPONENT EVENT (machine_state={state}, event_cid={cid}, event_state={event['state']}, event_transition_state={event['t_state']})")

def handle_engine_event(topic, msg):

		if "data" in msg:

			for d in msg['data']:

				# TS IN NANO SECONDS => * 1000000
				ts = d['ts'] * 1000000

				event_msg = d['msg']

				execution = d['execution']
				channel = execution['channel']
				source = execution['source']

				workUnit = execution['workUnit']

				if workUnit and workUnit['workUnitId'] and workUnit['workUnitId'] != "wid":

					logger.info(f"RRRR ENGINE EVENT SCRIPT RUN EXECUTION (source={source}, ts={ts}, channel={channel}, msg={event_msg}, work_unit={workUnit})")

				else:

					logger.info(f"SSSS EVENT SCRIPT EXECUTION (source={source}, ts={ts}, channel={channel}, msg={event_msg})")

def handle_task_event(topic, msg):

	for event in msg['events']:

		# #### META #### #

		meta = None

		if "meta" in event:

			meta: str = event['meta']

		if 'task::id' in meta:

			done = meta['task::done']
			t_state = event['t_state']

			task_description = f"task::id={meta['task::id']}, task::command={meta['task::command']}, task::done={done}, task::executor={meta['task::executor']}, task::started={meta['task::started']}, task::processed={meta['task::processed']}, task::meta={meta['task::meta']}"

			logger.info(f"(EEEE TASK {t_state} (event_ts={event['ts']}, state={event['state']}, msg={event['msg']}, {task_description})")

		# else:

		# 	logger.info(f"TTTT TASK EVENT (event_ts={event['ts']}, state={event['state']}, t_state={event['t_state']}, msg={event['msg']})")

def handle_cmd_counts_event(topic, msg):

	for d in msg['data']:
		value = d['value']

		if value:

			value_object = json.loads(value)

			if "counts" in value_object:

				logger.info(f"CCCC TASK COUNTS (counts={value_object['counts']})")

async def execute(login_data, task):

	 # #### create client with factory method #### #

	client = await AconitySTUDIOPythonClient.create(login_data, studio_version=task['studio_version'])

	# #### LOG SETUP #### #

	# * Create a logfile for this session in the example folder.
	# * The logfile contains the name of this script with a timestamp.
	# * Note: This is just one possible way to log.
	# * Logging configuration should be configured by the user, if any logging is to be used at all.

	client.log_setup(sys.argv[0], directory_path='')

	machine_name_ = task['machine_name']

	logger.info('######## ADD PROCESSORS ########')

	client.data.add_processor(['MachineState'], handle_machine_state_event)
	client.data.add_processor(['cmds'], handle_cmd_counts_event)
	client.data.add_processor(['task'], handle_task_event)
	client.data.add_processor(['run'], handle_engine_event)

	logger.info('######## SUBSCRIBE TOPICS ########')

	await client.data.subscribe_event_topic('MachineState')

	await client.data.subscribe_event_topic('cmds')
	await client.data.subscribe_event_topic('task')
	await client.data.subscribe_engine_topic('run')

	run = True

	while(run):

		logger.info("STILL WATCHING ... ")

		await asyncio.sleep(5)


#########################################################################################################################

if __name__ == '__main__':

	'''

	Example script on how to

	* use the AconitySCRIPT example to control the flow of a job
	* and combine it with a second client to analyse and execute commands
	* start this example before the job is started

	While job is paused, we show how to change its parameters.

	'''

	# #### change login_data to your needs #### #

	login_data = {
		'rest_url' : f'http://localhost:9000',
		'ws_url' : f'ws://localhost:9000',
		'email' : 'admin@aconity3d.com',
		'password' : 'passwd'
	}

	# #### change info to your needs #### #

	# info = {
	#     'machine_name' : 'SIM',
	#     'config_name': 'ACONITYMIDIplus_Simulation_Multi',
	#     'job_name': 'python_test',

	#     # AconitySTUDIO version 1.x.x or 2.x.x (or greater)?
	#     'studio_version' : 2
	# }

	info = {
		'machine_name' : 'Test',
		'config_name': '500217_ACONITYMINI_DAP_3D_Heated_No_Monitoring',
		'job_name': 'TT2',

		# AconitySTUDIO version 1.x.x or 2.x.x (or greater)?
		'studio_version' : 2
	}

	# info = {
	#     'machine_name' : 'Sim',
	#     'config_name': 'Synced_Multiscanner_Simulator',
	#     'job_name': 'python_test',

	#     # AconitySTUDIO version 1.x.x or 2.x.x (or greater)?
	#     'studio_version' : 2
	# }



	count = 0

	while(count < 5):

		try:

			logger.info("TRY TO EXECUTE")

			result = asyncio.run(execute(login_data, info), debug=True)

			logger.info(f"EXEC {result}")

		except Exception as e:

			count += 1
			logger.error(f"EXCEPTION WHILE EXECUTE (try_reconnect={count}) \n{e}")
