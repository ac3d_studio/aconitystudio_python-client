import asyncio

import sys

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient

# #### LOGGING #### #

import logging

_log = logging.getLogger(__name__)

async def pauser(pause, message):

	'''helper function to put messages on the console'''

	print('')

	for i in range(pause):

		sys.stdout.write(f'\rWaiting for {pause-i} seconds {message}')

		sys.stdout.flush()

		# simulating waiting time
		await asyncio.sleep(1)

	print('')

def handle_task_event(topic, msg):

	log = f"{topic} {msg['id']} {msg['report']}"

	for event in msg['events']:

		log  +=  f" {event['state']}\n{event['t_state']}\n{event['msg']}"

	_log.info(f"++++++++ ANALYSE TASK EVENTS (topic={topic})\n{log}")

def handle_cmds_event(topic, msg):

	log = f"{topic} {msg}"

	_log.info(f"******** ANALYSE CMDS EVENTS (topic={topic})\n{log}")

def handle_state_data(topic, msg):

	log = f"{topic} {msg['id']}"

	for data_msg in msg['data']:

		log  +=  f" {data_msg['cid']}"
		log  +=  f" {data_msg['ts']}"
		# log  +=  f"{data['tag']}\n"

		for data in data_msg['data']:

			log  +=  f" {data['name']} {data['type']} {data['value']}\n"

	_log.info(f"######## STATE (topic={topic})\n{log}")

async def execute(login_data, info):

	'''
	Example Code to illustrate on how a parameter can be changed during a job.
	The code does the folllowing things:
	* Gather necessary information about the machine and setup.
	* Start a job.
	* Wait for some database input (simulated by waiting)
	* Pause the job (after that, wait until state of machine is paused).
	* Change one global and two part specific parameters.
	* Resume the job.
	* After a little while, we stop the job.
	'''

	_log.info('######## EXAMPLE 2 "EXECUTE SCRIPT" ######## #')


	# #### CREATE CLIENT #### #

	_log.info('#### INIT CLIENT ####')

	client = await AconitySTUDIOPythonClient.create(login_data, info['studio_version'])

	# #### SESSION STATE #### #

	machine_id = await client.gateway.get_machine_id(info['machine_name'])
	config_id = await client.gateway.get_config_id(info['config_name'])
	job_id = await client.job.get_job_id(info['job_name'])

	# #### PROCESSORS #### #

	print('#### ADD PROCESSORS ####n')

	client.data.add_processor(['task'], handle_task_event)
	client.data.add_processor(['cmds'], handle_cmds_event)

	# client.data.add_processor(['State'], handle_state_data)

	print('#### SUBSCRIBE TOPICS ####')

	await client.data.subscribe_event_topic('task')
	await client.data.subscribe_event_topic('cmds')

	# await client.data.subscribe_data_topic('State')

	# #### START JOB #### #

	print('#### START JOB ####')

	execution_script = client.job.EXECUTION_SCRIPTS['only_expose']

	build_parts = 'all'
	start_layer = 1
	end_layer = 67

	await client.job.start_job(job_id=job_id, execution_script = execution_script, layers = [start_layer, end_layer], parts = build_parts)


	# ######## PAUSE JOB ######## #


	print('#### PAUSE JOB ####')

	# (The pauser coroutine does a simple asyncio.sleep,
	# but with a countdown printed on the console.)
	await pauser(15, message='until we initiate a pause ...')

	print('\n\t*** INIT PAUSE (machine will be paused when the current layer is finished, which may take a few moments.)***')

	await client.job.pause_job(job_id=job_id)

	await asyncio.sleep(5)

	# ######## JOB PAUSED ######## #


	print('\n\n ######## JOB PAUSED ########\n\n')

	# #### CHANGE GLOBAL PARAMETER #### #

	# # Next, we change one global parameter and two part specific parameters.
	# # (To see the changed parameters in the GUI please reload the tab.)

	await client.job.change_global_parameter(job_id, 'travel_velocity', 113)

	# #### CHANGE PART PARAMETER #### #

	# # Note: for the (first) argument of change_part_parameter, please read the documentation.

	part_id = 1
	param = '0_master_panel_mark_speed'
	value = 4
	await client.job.change_part_parameter(job_id, part_id = part_id, param=param, value=value)

	# #await client.change_part_parameter(*change_2)
	# #change_2 = (2, 'mark_speed', 456)
	# print('\n\t***Parameters were changed. (To see the result in in the GUI, please reload the tab.)***\n\n')


	# ######## RESUME JOB ######## #


	# Resume the Job. We want to start at a different layer now, depending on how many layers we already built.
	print('\n\t***Next, we calculate the new starting layer and resume the job.***\n\n')

	new_starting_layer = client.job.job_info['start_layer'] + client.job.job_info['AddLayerCommands']

	await asyncio.sleep(5)
	await client.job.resume_job(job_id, layers=[new_starting_layer, end_layer], parts=build_parts)


	# ######## STOP JOB ######## #


	#After a little while, we stop the job and exit.
	print('\n\t***We soon stop the job and exit***')
	await pauser(10, message = 'until we stop the job ...')

	# await client.pause_job()

	await client.job.stop_job(job_id) # use this line instead of pause_job to initiate a hard stop

	await pauser(10, message = 'until we exit ...')
	# ######## DONE ######## #


	print('\n\t***we are done***\n\n')

#########################################################################################################################

if __name__ == '__main__':

	'''
	Example script on how to use the AconitySCRIPT to control the flow of a job.
	While job is paused, we show how to change its parameters.
	'''

	# #### LOGIN DATA #### #

	login_data = {
		'rest_url' : f'http://localhost:9000',
		'ws_url' : f'ws://localhost:9000',
		'email' : 'admin@aconity3d.com',
		'password' : 'passwd'
	}

	# #### SESSION STATE #### #

	info = {
		'machine_name' : 'Test',
		'config_name' : 'ACONITYTWO_Simulation_Multi',
		'job_name': 'ScriptTest2',
		'studio_version' : 2
	}

	result = asyncio.run(execute(login_data, info), debug=True)
