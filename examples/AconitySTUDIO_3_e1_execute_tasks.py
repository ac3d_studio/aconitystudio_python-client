import asyncio

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient

# #### LOGGING #### #

import logging

_log = logging.getLogger(__name__)


# ######## EXAMPLE 1 "EXECUTE TASKS" ######## #


# ######## PROCESSOR FUNCTIONS ######## #


def handle_task_event(topic, msg):

	log = ""

	for event in msg['events']:

		log  += f"    {event['state']} -> {event['t_state']} -> {event['msg']}\n"

	_log.info(f"++++++++ ANALYSE TASK EVENTS (topic={topic}, mid={msg['id']}, report={msg['report']}) ++++++++\n{log}")

def handle_cmds_event(topic, msg):

	log = f"    {msg}\n"

	_log.info(f"******** ANALYSE CMDS EVENTS (topic={topic}) ********\n{log}")

def handle_positioning_data(topic, msg):

	log = f"{msg['id']}"

	for data_msg in msg['data']:

		log  +=  f" {data_msg['ts']}"
		log  +=  f" [{data_msg['cid']}]\n"
		# log  +=  f"{data['tag']}\n"

		for data in data_msg['data']:

			log  +=  f"    {data['name']}: {data['type']} = {data['value']}\n"

	_log.info(f"######## POS (topic={topic}) ########\n{log}")

def handle_state_data(topic, msg):

	log = f"{topic} {msg['id']}"

	for data_msg in msg['data']:

		log  +=  f" {data_msg['ts']}"
		log  +=  f" [{data_msg['cid']}]\n"
		# log  +=  f"{data['tag']}\n"

		for data in data_msg['data']:

			log  +=  f"    {data['name']}: {data['type']} = {data['value']}\n"

	_log.info(f"######## STATE (topic={topic}) ########\n{log}")


# ######## EXECUTION FUNCTION ######## #


async def execute(login_data, info):

	'''
	Example code demonstrating simple usages of the Python client task api.

	1) asyncioTask object "lighter" turns lights on and off
	2) Move Slider, Supplier tasks are also available.
	'''

	_log.info('######## EXAMPLE 1 "EXECUTE TASKS" ######## #')

	_log.info('#### INIT CLIENT ####')

	#create client with factory method
	client = await AconitySTUDIOPythonClient.create(login_data, studio_version=info['studio_version'])

	# #### SESSION STATE #### #

	_log.info('#### CHECK SESSION STATE ####')

	# #### MACHINE #### #

	machine_name = info['machine_name']

	machine_id = await client.gateway.get_machine_id(machine_name)

	# #### (START) CONFIG #### #

	config_name = info['config_name']

	config_id = await client.gateway.get_config_id(config_name)

	if await client.gateway.config_state(config_id) == "inactive":

		await client.gateway.start_config(config_id)

	else:

		_log.info("CONFIG IS ALREADY STARTED!")

	# #### JOB #### #

	job_name = info['job_name']

	job_id = await client.job.get_job_id(job_name)

	# #### PROCESSORS #### #

	_log.info('#### ADD PROCESSORS ####')

	client.data.add_processor(['task'], handle_task_event)
	client.data.add_processor(['cmds'], handle_cmds_event)

	# client.data.add_processor(['Positioning'], handle_positioning_data)
	# client.data.add_processor(['State'], handle_state_data)


	_log.info('#### SUBSCRIBE TOPICS ####')

	await client.data.subscribe_event_topic('task')
	await client.data.subscribe_event_topic('cmds')

	# await client.data.subscribe_data_topic('Positioning')
	# await client.data.subscribe_data_topic('State')

	# #### TASK EXECUTION #### #

	_log.info('######## START TASK EXECUTION ... ########')

	# #### EXPOSE #### #

	_log.info('#### TRY expose ####')

	await client.task.expose(machine_id, job_id, 2, [1,2,3,4], channel="run0")

	_log.info('\n #### TRYED expose ####\n')

	# # #### LIGHT #### #

	# _log.info('#### TRY on/off "light" ####')

	# if(await client.task.on(machine_id, "light")):

	# 	_log.info('#### on "light" => wait ####\n')

	# 	await asyncio.sleep(5)

	# 	if(await client.task.off(machine_id, "light")):

	# 		_log.info('#### off "light" ####\n')

	# #### GUIDE BEAM 1 #### #

	_log.info('#### TRY on/off "guide_beam_1" ####')

	if(await client.task.on(machine_id, "guide_beam_1")):

		_log.info('#### on "guide_beam_1" => wait ####')

		await asyncio.sleep(5)

		if(await client.task.off(machine_id, "guide_beam_1")):

			_log.info('#### off "guide_beam_1" ####')

	# # #### SUPPLIER #### #

	# _log.info('#### TRY move_rel "supplier_1" +3/-3 #### ')

	# if(await client.task.move_rel(machine_id, "supplier_1", 3)):

	# 	_log.info('#### moved "supplier_1" +3 ####')

	# 	if(await client.task.move_rel(machine_id, "supplier_1", -3)):

	# 		_log.info('#### moved "supplier_1" -3 ####')

	# # #### SLIDER #### #

	# _log.info('#### TRY move_rel "slider" +10/-10 #### ')

	# if(await client.task.move_rel(machine_id, "slider", 10)):

	# 	_log.info('#### moved "slider" +10 ####')

	# 	if(await client.task.move_rel(machine_id, "slider", -10)):

	# 		_log.info('#### moved "slider" -10 ####')

	# # #### OPTICAL AXIS #### #

	# _log.info('#### TRY move_rel "optical_axis" +30/-30 ####')

	# if(await client.task.move_rel(machine_id, "optical_axis", 30)):

	# 	_log.info('#### moved "optical_axis" +30 ####')

	# 	if(await client.task.move_rel(machine_id, "optical_axis", -30)):

	# 		_log.info('#### moved "optical_axis" -30 ####')

	# await asyncio.sleep(5)

	_log.info('######## ... TASKS EXECUTED ########')


# ######## MAIN ######## #


if __name__ == '__main__':


	'''
	Example on how to use the python client for executing scripts.
	Please change login_data and info to your needs.
	'''

	# #### LOGIN DATA #### #

	login_data = {
		'rest_url' : f'http://localhost:9000',
		'ws_url' : f'ws://localhost:9000',
		'email' : 'admin@aconity3d.com',
		'password' : 'passwd'
	}

	# #### SESSION STATE #### #

	info = {
		'machine_name' : 'Test',
		'config_name' : 'ACONITYTWO_Simulation_Multi',
		'job_name': 'ScriptTest',
		'studio_version' : 2
	}

	# #### RUN EXECUTION FUNCTION #### #

	result = asyncio.run(execute(login_data, info), debug=True)
