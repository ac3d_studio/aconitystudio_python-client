# AconitySTUDIOpy

version number: 3.0.11

author: Hendrik Blom

------------------

## Overview

A python client to control AconitySTUDIO.

This client is useful for automating tasks, like observing machine data or changing parameters of a job in reaction to incoming data.

See the examples section for usage.

------------------

## Setup

### Install Package (Default)

Download the packaged python client as [python wheel](https://pythonwheels.com/) from the Download tab in BitBucket and run

    $ pip install AconitySTUDIO_client.whl

### Build Package (Alternative)

Clone the repo:

    $ git clone git@bitbucket.org:ac3d_studio/aconitystudio_python-client.git

    $ cd aconitystudio_python-client

Install requirements:

    $ pip install -r requirements.txt

Use locally

    $ python setup.py install

or package the python client as a wheel (wheel package needs to be installed):

    $ python setup.py sdist bdist_wheel

------------------

## Documentation

To create the documentation, change into the `docs` folder and run

    $ python -msphinx -M html . _build

If you have make, you can instead simply run

    $ make html

Start `_build/html/index.html` in your browser to read the docs.

    $ firefox.exe _build/html/index.html

------------------

## Contributing

Hendrik Blom

------------------

## Examples

The examples folder contains some standard use cases.

Before starting the scripts, please configure the `login_data` and `info` dictionaries in the python script to your needs.

More detailed information about the following scripts and what they can do is contained in the files itself.

For more information on each example, please also take a look into the sourcecode of these files.



`AconitySTUDIO_3_e1_execute_tasks.py`

* Showcases an example on how to use the python client to generate tasks with AconitySCRIPT to control the machine.

Contains commented out code with which to control the Slider and Supplier.

For learning how to use the python client, this is a good place to start.

`AconitySTUDIO_3_e2_AconitySCRIPT.py`

* This example showcases in detail on how to use the client to manipulate jobs.
* The script will start a job, pause it, change some parameters and finally resume the job.
* It exemplifies how to use the job API (`pause_job`, `resume_job`, `start_job`, `stop_job`).

`AconitySTUDIO_3_e3_get_batch_data.py`

* Example on how to continually download the pyrometer data.
* This script will periodically check if new data is available on the server and download it to a local folder
* To specify where the files shall be downloaded, control the quality (in percent, downsampling parameter) and configure if the Pyrometer or Profiler (or both) data should be downloaded please open the script in a text editor and customize the `info` dictionary at the bottom of the script.
* Please note that for each started session, started config and started job there exists individual data. This means, `session_id`, `config_id` and `job_id` influence which files will be downloaded. Note that inside `analyse_workunit_metadata` if statements can be disabled so that all/more data is downloaded, for example all data from one session.
* This example script is in beta still, it has not been extensively tested.

`AconitySTUDIO_3_e4_resume_after_subprocess_on_pause.py`

* This example showcases in detail on how to use the client to manipulate jobs by using an external program to generate tasks.
* The script will wait until the job is paused, change some parameters and finally resume the job.

`AconitySTUDIO_3_e4_restart_after_subprocess_on_pause.py`

* This example showcases in detail on how to use the client to manipulate jobs by using an external program to generate tasks.
* The script will wait until the job is paused, stop the job, change some parameters and finally restart the job.
