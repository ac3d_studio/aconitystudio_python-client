import time

from AconitySTUDIOpy import ConnectionAPI
from AconitySTUDIOpy import ExecutionAPI
from AconitySTUDIOpy import GatewayAPI
from AconitySTUDIOpy import DataAPI
from AconitySTUDIOpy import JobAPI
from AconitySTUDIOpy import TaskAPI

# #### LOGGING #### #

import logging

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')

_log = logging.getLogger(__name__)


# ######## AconitySTUDIO_client ######## #


class AconitySTUDIO_client:

	'''
	The AconitySTUDIO Python Client. Allows for easy automation and job
	management.

	For example usages, please consult the examples folder
	in the root directory from this repository.

	To create the client call the `classmethod` create.
	'''

	def __init__(self, connection_api, gateway_api, execution_api, data_api, job_api, task_api, time_zone="Europe/Berlin"):

		# DEFAULT VERSION (STUDIO SERVER IS AT LEAST VERSION 2 OR HIGHER)

		self.studio_version = 2

		# #### CONNECTION #### #

		self.connection = connection_api
		self.data = data_api
		self.gateway = gateway_api

		# #### CONTROL ##### #

		self.execution = execution_api
		self.task = task_api
		self.job = job_api

		# #### TIMEOUTS #### #

		self.time_out_script_routes = 5

		# #### DEFINE TIME #### #

		self.time_zone = time_zone

		self.start_time = time.time()

	def __str__(self):

		'''
		Print out some information about the client
		'''

		myself = f'\t\t\tLast {len(self.connection.history)} requests:\n'

		for request in self.connection.history:

			myself += f'\t\t\t\t{request}\n'

		myself += f'\t\t\tInfo:'

		infos = ['machine_name', 'machine_id', 'config_name', 'config_id', 'job_name', 'job_id', 'workunit_id', 'session_id']

		for info in infos:

			information = getattr(self, info, 'Not available')

			myself += f'\n\t\t\t\t{info}: {information}'

		return myself


	# ######## CREATE ######## #


	@classmethod
	async def create(cls, login_data, studio_version, time_zone="Europe/Berlin"):

		'''
		Factory class method to initialize a client.
		Convenient as this function takes care of logging in and creating a websocket connection.
		It will also set set up a ping, to ensure the connection will not be lost.

		:param login_data: required keys are `rest_url`, `ws_url`, `password` and `email`.
		:type login_data: dictionary

		:param studio_version: specify version 1/2/higher
		:type studio_version: int

		:param time_zone: optional timezone specification for timestamp conversion; default is "Europe/Berlin"
		:type time_zone: string

		Usage::

			login_data = {
				'rest_url' : 'http://192.168.1.1:2000',
				'ws_url' : 'ws://192.168.1.1:2000',
				'email' : 'admin@yourcompany.com',
				'password' : '<password>'
			}
			client = await AconitySTUDIO_client.create(login_data, 2)

		'''

		_log.info('TRY CREATE CLIENT ...')

		# #### ConnectionAPI #### #

		connection_api = ConnectionAPI.ConnectionAPI(login_data)

		await connection_api.connect()

		# #### DataAPI #### #

		data_api = DataAPI.DataAPI(connection_api)

		await data_api.connect()

		# #### GatewayAPI #### #

		gateway_api = GatewayAPI.GatewayAPI(connection_api)

		# #### ExecutionAPI #### #

		execution_api = ExecutionAPI.ExecutionAPI(connection_api)

		# #### JobAPI #### #

		job_api = JobAPI.JobAPI(connection_api, gateway_api, execution_api, studio_version)

		await job_api.connect()

		# #### TASK API #### #

		task_api = TaskAPI.TaskAPI(execution_api, job_api, studio_version)

		# #### CREATE CLIENT #### #

		self = AconitySTUDIO_client(connection_api, gateway_api, execution_api, data_api, job_api, task_api, time_zone)

		_log.info('... CLIENT CREATED ...')

		return self

	def log_setup(self, filename, directory_path=''):

		pass
