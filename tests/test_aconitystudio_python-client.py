# -*- coding: utf-8 -*-

"""Tests for `AconitySTUDIO_client` package."""

import unittest

from AconitySTUDIOpy.AconitySTUDIO_client import AconitySTUDIO_client as AconitySTUDIOPythonClient


class TestAconitySTUDIO_client(unittest.TestCase):
    """Tests for `AconitySTUDIO_client` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_something(self):
        """Test something."""


if __name__ == '__main__':
    unittest.main()
